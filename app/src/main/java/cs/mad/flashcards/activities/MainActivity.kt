package cs.mad.flashcards.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import cs.mad.flashcards.R
import cs.mad.flashcards.adapters.FlashcardSetAdapter
import cs.mad.flashcards.entities.FlashcardSet


/*
===================================================================================================================

     Reference documentation for recyclers: https://developer.android.com/guide/topics/ui/layout/recyclerview

===================================================================================================================
 */

class MainActivity : AppCompatActivity() {
    private var data = FlashcardSet.getHardcodedFlashcardSets().toMutableList()

    override fun onCreate(savedInstanceState: Bundle?) {
        /*
            connect to views using findViewById
            setup views here - recyclerview, button_delete
            don't forget to notify the adapter if the data set is changed
         */

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        GridLayoutManager(this,2)

        val recyclerView =findViewById<RecyclerView>(R.id.recycle_grid_main)
        recyclerView.adapter = FlashcardSetAdapter(data)

        val textView: TextView = findViewById(R.id.button_add_main)
        textView.setOnClickListener{addEle()}
    }

    private fun addEle() {

        data.add(FlashcardSet("term"))
        val recyclerView = findViewById<RecyclerView>(R.id.recycle_grid_main)
        recyclerView.adapter = FlashcardSetAdapter(data)
    }
}