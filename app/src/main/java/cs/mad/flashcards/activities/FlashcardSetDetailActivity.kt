package cs.mad.flashcards.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import cs.mad.flashcards.R
import cs.mad.flashcards.adapters.FlashcardAdapter
import cs.mad.flashcards.entities.Flashcard

class FlashcardSetDetailActivity : AppCompatActivity() {
    private var data = Flashcard.getHardcodedFlashcards().toMutableList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.other_view)

        LinearLayoutManager(this)

        val recyclerView =findViewById<RecyclerView>(R.id.recycle_lin)
        recyclerView.adapter = FlashcardAdapter(data)

        val textView: TextView = findViewById(R.id.button_detail_add)
        textView.setOnClickListener{addEle()}
    }

    private fun addEle() {

        data.add(Flashcard("term", "def"))
        val recyclerView =findViewById<RecyclerView>(R.id.recycle_lin)
        recyclerView.adapter = FlashcardAdapter(data)
    }
}